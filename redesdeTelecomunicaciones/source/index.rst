.. Redes de Telecomunicaciones documentation master file, created by
   sphinx-quickstart on Wed Feb  8 09:17:23 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Redes de Telecomunicaciones's documentation!
=======================================================

Contents:

.. toctree::
   ./glosario/index_glosario.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

