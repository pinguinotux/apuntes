NODO
====

Es todo aquello a lo que se le pueda asignar una dirección IP.

DATAGRAMA
=========

Un datagrama es un paquete de datos que constituye el mínimo bloque de 
información en una red de conmutación por datagramas, la cual es uno 
de los dos tipos de protocolo de comunicación por conmutación de 
paquetes usados para encaminar por rutas diversas dichas unidades de 
información entre nodos de una red, por lo que se dice que no está 
orientado a conexión. La alternativa a esta conmutación de paquetes es 
el circuito virtual, orientado a conexión.

El datagrama se divide en dos partes: El **Encabezado** y la **Carga útil**.

.. image:: ../imag/datagrama1.png


IP
==

Protocolo del menor esfuerzo.

OSI vs IP/TCP
=============

.. image:: ../imag/osi1.png
.. image:: ../imag/osi2.jpg
.. image:: ../imag/osi3.png

TCP
===

1. Permite colocar los datagramas nuevamente en orden cuando vienen del protocolo IP.
2. Permite el monitoreo del flujo de los datos y así evita la saturación de la red.
3. Permite que los datos se formen en segmentos de longitud variada para 
   "entregarlo" al protocolo IP.
4. Permite multiplexar los datos, es decir, que la información que viene de 
   diferentes fuentes (por ejemplo, aplicaciones) en la misma línea pueda circular 
   simultáneamente.
5. Por último, permite comenzar y finalizar la comunicación amablemente.


IPV4
====

Las direcciones IP contienen la siguiente información: **# de Red** y **# de Nodo** o
**Host**.

.. image:: ../imag/ip1.gif



.. image:: ../imag/ip2.gif

Clase A
+++++++

No se puede contar el x.0.0.0 ya que representa la red x.

El x.255.255.255 representa el *Broadcast* que tiene un significado diferente
al que se tiene en radio-difusión ya que a pesar de que la información pueda
llegar a todos, la dirección es direccionada solo a uno o a algunos, es decir,
discrimina, cosa que no sucede en el termino de Broadcast para radio.

**#Redes**: 2^7 - 2 = 126

**#Nodoes/ClaseA**: 2^24 - 2 = 16'777.214

Broadcast
=========

Como fue mencionado el significado de Broadcast es diferente al de radio, ya que no
hay difusión en el sentido de que a pesar de que llegue a todos, es discriminada.
Así entonces también se puede pensar en: **Multi**, **any**, **uni** ... cast.




