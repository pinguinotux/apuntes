NAT
===

**Nota**: NAT viola modelo OSI, perdida de compatibilidad.

La traducción de direcciones de red o NAT (del inglés Network Address Translation) 
es un mecanismo utilizado por routers IP para intercambiar paquetes entre dos 
redes que asignan mutuamente direcciones incompatibles. Consiste en convertir, en 
tiempo real, las direcciones utilizadas en los paquetes transportados. También es 
necesario editar los paquetes para permitir la operación de protocolos que 
incluyen información de direcciones dentro de la conversación del protocolo.

El tipo más simple de NAT proporciona una traducción una-a-una de las direcciones 
IP. La RFC 2663 se refiere a este tipo de NAT como NAT Básico, también se le 
conoce como NAT una-a-una. En este tipo de NAT únicamente, las direcciones IP, las 
sumas de comprobación (checksums) de la cabecera IP, y las sumas de comprobación 
de nivel superior, que se incluyen en la dirección IP necesitan ser cambiadas. 
El resto del paquete se puede quedar sin tocar (al menos para la funcionalidad 
básica del TCP/UDP, algunos protocolos de nivel superior pueden necesitar otra forma 
de traducción). Es corriente ocultar un espacio completo de direcciones IP, 
normalmente son direcciones IP privadas, detrás de una única dirección IP (o pequeño 
grupo de direcciones IP) en otro espacio de direcciones (normalmente público).

NAT es como el recepcionista de una oficina grande. Imagine que le indica al 
recepcionista que no le pase ninguna llamada a menos que se lo solicite. Más tarde, 
llama a un posible cliente y le deja un mensaje para que le devuelva el llamado. A 
continuación, le informa al recepcionista que está esperando una llamada de este 
cliente y le solicita que le pase la llamada a su teléfono.

El cliente llama al número principal de la oficina, que es el único número que el 
cliente conoce. Cuando el cliente informa al recepcionista a quién está buscando, el 
recepcionista se fija en una tabla de búsqueda que indica cuál es el número de e
xtensión de su oficina. El recepcionista sabe que el usuario había solicitado esta 
llamada, de manera que la reenvía a su extensión.

Entonces, mientras que el servidor de DHCP asigna direcciones IP dinámicas a los 
dispositivos que se encuentran dentro de la red, los routers habilitados para NAT 
retienen una o varias direcciones IP de Internet válidas fuera de la red. Cuando el 
cliente envía paquetes fuera de la red, NAT traduce la dirección IP interna del 
cliente a una dirección externa. Para los usuarios externos, todo el tráfico que 
entra a la red y sale de ella tiene la misma dirección IP o proviene del mismo 
conjunto de direcciones.

Su uso más común es permitir utilizar direcciones privadas (definidas en el RFC 1918) 
para acceder a Internet. Existen rangos de direcciones privadas que pueden usarse 
libremente y en la cantidad que se quiera dentro de una red privada. Si el número 
de direcciones privadas es muy grande puede usarse solo una parte de direcciones 
públicas para salir a Internet desde la red privada. De esta manera simultáneamente 
sólo pueden salir a Internet con una dirección IP tantos equipos como direcciones 
públicas se hayan contratado. Esto es necesario debido al progresivo agotamiento de 
las direcciones IPv4. Se espera que con el advenimiento de IPv6 no sea necesario 
continuar con esta práctica.

.. image:: ../imag/nat1.png

**Funcionamiento**

El protocolo TCP/IP tiene la capacidad de generar varias conexiones simultáneas con un 
dispositivo remoto. Para realizar esto, dentro de la cabecera de un paquete IP, 
existen campos en los que se indica la dirección origen y destino. Esta combinación de 
números define una única conexión.

La mayoría de los NAT asignan varias máquinas (hosts) privadas a una dirección IP 
expuesta públicamente. En una configuración típica, una red local utiliza unas 
direcciones IP designadas “privadas” para subredes (RFC 1918). Un ruteador en esta red 
tiene una dirección privada en este espacio de direcciones. El ruteador también está 
conectado a Internet por medio de una dirección pública asignada por un proveedor de 
servicios de Internet. Como el tráfico pasa desde la red local a Internet, la dirección 
de origen en cada paquete se traduce sobre la marcha, de una dirección privada a una 
dirección pública. El ruteador sigue la pista de los datos básicos de cada conexión 
activa (en particular, la dirección de destino y el puerto). Cuando una respuesta llega 
al ruteador utiliza los datos de seguimiento de la conexión almacenados en la fase de 
salida para determinar la dirección privada de la red interna a la que remitir la respuesta.

Todos los paquetes de Internet tienen una dirección IP de origen y una dirección 
IP de destino. En general, los paquetes que pasan de la red privada a la red 
pública tendrán su dirección de origen modificada, mientras que los paquetes 
que pasan a la red pública de regreso a la red privada tendrán su dirección 
de destino modificada. Existen configuraciones más complejas.

Para evitar la ambigüedad en la forma de traducir los paquetes de vuelta, es 
obligatorio realizar otras modificaciones. La mayor parte del tráfico generado 
en Internet son paquetes TCP y UDP, para estos protocolos los números de 
puerto se cambian, así la combinación de la información de IP y puerto en el 
paquete devuelto puede asignarse sin ambigüedad a la información de dirección
 privada y puerto correspondiente. Los protocolos que no están basados en TCP 
 y UDP requieren de otras técnicas de traducción Los paquetes ICMP normalmente
  se refieren a una conexión existente y necesitan ser asignado utilizando 
  la misma información de IP. Para el ICMP al ser una conexión existente no 
  se utiliza ningún puerto.

Una pasarela NAT cambia la dirección origen en cada paquete de salida y, dependiendo 
del método, también el puerto origen para que sea único. Estas traducciones de 
dirección se almacenan en una tabla, para recordar qué dirección y puerto le 
corresponde a cada dispositivo cliente y así saber donde deben regresar los paquetes 
de respuesta. Si un paquete que intenta ingresar a la red interna no existe en 
la tabla en un determinado puerto y dirección se puede acceder a un determinado 
dispositivo, como por ejemplo un servidor web, lo que se denomina NAT inverso 
o DNAT (Destination NAT).

NAT tiene muchas formas de funcionamiento, entre las que destacan:

**Nat Estático**

Conocida también como NAT 1:1, es un tipo de NAT en el que una dirección 
IP privada se traduce a una dirección IP pública, y donde esa dirección 
pública es siempre la misma. Esto le permite a un host, como un servidor Web, 
el tener una dirección IP de red privada pero aun así ser visible en Internet.

**Dinámica**

Es un tipo de NAT en la que una dirección IP privada se mapea a una IP pública 
basándose en una tabla de direcciones de IP registradas (públicas). Normalmente,
 el router NAT en una red mantendrá una tabla de direcciones IP registradas, 
 y cuando una IP privada requiera acceso a Internet, el router elegirá una 
 dirección IP de la tabla que no esté siendo usada por otra IP privada. 
 Esto permite aumentar la seguridad de una red dado que enmascara la configuración 
 interna de una red privada, lo que dificulta a los hosts externos de la red el 
 poder ingresar a ésta. Para este método se requiere que todos los hosts de la 
 red privada que deseen conectarse a la red pública posean al menos una IP pública asociadas.
 
**Sobrecarga**

La más utilizada es la NAT de sobrecarga, conocida también como PAT 
(Port Address Translation - Traducción de Direcciones por Puerto), NAPT 
(Network Address Port Translation - Traducción de Direcciones de Red por Puerto)
, NAT de única dirección o NAT multiplexado a nivel de puerto.

**Solapamiento**

Cuando las direcciones IP utilizadas en la red privada son direcciones 
IP públicas en uso en otra red, el encaminador posee una tabla de traducciones 
en donde se especifica el reemplazo de éstas con una única dirección IP pública. 
Así se evitan los conflictos de direcciones entre las distintas redes.


**Tipos de NAT**

Los dispositivos NAT no tienen un comportamiento uniforme y se ha tratado 
de clasificar su uso en diferentes clases. Existen cuatro tipos de NAT:

    NAT de cono completo (Full-Cone NAT). En este caso de comunicación completa, NAT mapeará la dirección IP y puerto interno a una dirección y puerto público diferentes. Una vez establecido, cualquier host externo puede comunicarse con el host de la red privada enviando los paquetes a una dirección IP y puerto externo que haya sido mapeado. Esta implementación NAT es la menos segura, puesto que una atacante puede adivinar qué puerto está abierto.
    NAT de cono restringido (Restricted Cone NAT). En este caso de la conexión restringida, la IP y puerto externos de NAT son abiertos cuando el host de la red privada quiere comunicarse con una dirección IP específica fuera de su red. El NAT bloqueará todo tráfico que no venga de esa dirección IP específica.
    NAT de cono restringido de puertos (Port-Restricted Cone NAT). En una conexión restringida por puerto NAT bloqueará todo el tráfico a menos que el host de la red privada haya enviado previamente tráfico a una IP y puerto especifico, entonces solo en ese caso ésa IP:puerto tendrán acceso a la red privada.
    NAT Simétrico (Symmetric NAT). En este caso la traducción de dirección IP privada a dirección IP pública depende de la dirección IP de destino donde se quiere enviar el tráfico.

SQAL


**NAT simétrico**

Según se encuentra en RFC 3489(sección 5),1 de estos tipos de NAT que existen, NAT simétrico es el más difícil con el que trabajar. Esto es debido a que el mapeo entre IP y puerto privado contra IP y puerto público no se conserva, es decir por cada requerimiento saliente se asigna un puerto aleatorio y éste varía para cada comunicación. En los tres anteriores tipos de NAT, la dirección IP y el puerto interno de la comunicación en el equipo que realiza NAT se conservaba independientemente de la dirección: puerto externo del host de destino, pero en este caso varía y adicionalmente conserva las restricciones del Port Restricted Cone NAT. Por lo tanto cuando el host destino intenta enviar un paquete a la dirección origen, el router NAT rechazaría el paquete porque no reconoce al que envía el paquete como un host “autorizado” a enviar a esa dirección. La ventaja del NAT simétrico, es que varias máquinas internas con el mismo puerto origen, pueden establecer comunicaciones al mismo tiempo y por este motivo, este tipo de NAT es una buena opción cuando varias máquinas internas salen por el mismo Gateway, el cual solo tiene una IP pública y adicionalmente las aplicaciones de estos equipos originen un requerimiento desde el mismo puerto. Este tipo de NAT es común en aplicaciones o dispositivos que implementan el protocolo SIP, donde el puerto origen y destino típicamente es el 5060. Sin embargo en un método reciente de clasificación, explicado en el RFC 4787, se distinguen los tipos de NAT por dos propiedades: según la forma de mapear y según la forma de filtrar. Y pueden ser de tres tipos: Independiente del punto destino (Endpoint-Independent), dependiente de la dirección (Address-Dependent), o dependiente de la dirección y del puerto (Address and Port-Dependent). Con este nuevo método de clasificación, NAT simétrico en realidad sería un NAT que mapea dependiendo de la dirección y el puerto.
