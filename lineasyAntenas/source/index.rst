.. notas Lineas y Antenas documentation master file, created by
   sphinx-quickstart on Thu Feb  9 09:48:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to notas Lineas y Antenas's documentation!
==================================================

Contents:

.. toctree::
   ./lineas/index_lineas.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

